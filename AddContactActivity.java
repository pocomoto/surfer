package com.example.itoma.test001;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.itoma.test001.database.DatabaseHandler;
import com.example.itoma.test001.database.model.Contact;
import com.example.itoma.test001.database.model.User;

public class AddContactActivity extends AppCompatActivity {
    EditText mName, mPhone;
    DatabaseHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        mName = (EditText) findViewById(R.id.add_cont_name);
        mPhone = (EditText) findViewById(R.id.add_cont_phone);

        mHandler = new DatabaseHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            doAdd();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doAdd() {
        String name = mName.getText().toString();
        String phone = mPhone.getText().toString();

        if (!isValidName(name)) {
            mName.setError("Name is not valid!");
        } else if (!isValidPhone(phone)) {
            mPhone.setError("Phone Number is not valid!");
        } else {
            Contact contact = new Contact(name, phone);
            try {
                Contact.add(mHandler, contact);

                setResult(RESULT_OK);
                finish();
            } catch (Exception e) {
                Snackbar.make(mName, "Error: " + e.getMessage(), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public boolean isValidName(String name) {
        return !name.isEmpty() && name.length() >= 6;
    }

    public boolean isValidPhone(String phone) {
        return !phone.isEmpty() && phone.length() >= 6;
    }
}
