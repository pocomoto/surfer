package com.example.itoma.test001;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.itoma.test001.database.DatabaseHandler;
import com.example.itoma.test001.database.model.User;

public class RegisterActivity extends AppCompatActivity {
    EditText mEmail, mPass, mConf;
    DatabaseHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEmail = (EditText) findViewById(R.id.reg_email);
        mPass = (EditText) findViewById(R.id.reg_pass);
        mConf = (EditText) findViewById(R.id.reg_pass_conf);

        mHandler = new DatabaseHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_register) {
            doRegister();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doRegister() {
        String email = mEmail.getText().toString();
        String pass = mPass.getText().toString();
        String conf = mConf.getText().toString();

        if (!isValidEmail(email)) {
            mEmail.setError("Email not valid!");
        } else if (!isValidPassword(pass, conf)) {
            mPass.setError("Password not valid!");
        } else {
            User user = new User(email, pass);
            try {
                User.add(mHandler, user);

                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                finish();
            } catch (Exception e) {
                Snackbar.make(mEmail, "Error: " + e.getMessage(), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public boolean isValidEmail(String email) {
        return !email.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPassword(String pass, String conf) {
        return !pass.isEmpty() && pass.length() >= 6 && pass.equals(conf);
    }
}
